from news.models import Article, Like
from news.serializers import UserSerializer, ArticleSerializer, LikeSerializer, ArticleSummarySerializer
from rest_framework import mixins, generics, status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User


class ArticleList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSummarySerializer
    """Class used for managing articles"""

    def get(self, request, *args, **kwargs):
        """
        @api {get} /articles/ Request Article list
        @apiName ArticleList
        @apiGroup Article

        @apiSuccess {Article[]} List of articles
        """
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        @api {post} /articles/ Request Article list
        @apiName NewArticle
        @apiGroup Article

        @apiSuccess (201) {Article[]} List of articles
        """
        return self.create(request, *args, **kwargs)


class ArticleDetail(LoginRequiredMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    generics.GenericAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    raise_exception = True
    permission_denied_message = "You are not authorized to access this Article"

    def get(self, request, *args, **kwargs):
        """
        @api {get} /articles/:id/ Request Article
        @apiName Article
        @apiGroup Article

        @apiSuccess {Article} Instance of Article
        """
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """
        @api {put} /articles/:id/ Modify Article
        @apiName Article
        @apiGroup Article

        @apiSuccess {Object} article Instance of Article
        """
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """
        @api {delete} /articles/:id/ Delete Article
        @apiName Article
        @apiGroup Article

        @apiSuccess 204 article Instance of Article
        """
        return self.destroy(request, *args, **kwargs)


class LikeView (LoginRequiredMixin,
                generics.GenericAPIView):
    """ Class used for managing adding and removing likes """

    raise_exception = True
    permission_denied_message = "You are not authorized to like this Article"

    def get_article(self, pk):
        try:
            return Article.objects.get(pk=pk)
        except Article.DoesNotExist:
            raise status.HTTP_404_NOT_FOUND

    def put(self, request, pk):
        """
        @api {put} /articles/:id/like/ Like an article
        @apiName Like
        @apiGroup Article

        @apiSuccess {None}
        """
        article = self.get_article(pk)
        like_data = {
            'article': article.pk,
            'user': request.user.pk,
        }
        serializer = LikeSerializer(data=like_data)
        if serializer.is_valid():
            serializer.save(article_id=article.pk, user_id=request.user.pk)
            article.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        """
        @api {delete} /articles/:id/like/ Delete like for Article
        @apiName Unlike
        @apiGroup Article

        @apiSuccess {Object} like Instance of Like
        """
        article = self.get_article(pk)
        like = Like.objects.filter(article=article, user=request.user)
        like.delete()
        article.save()
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, pk):
        """
        @api {get} /articles/:id/like/ get like for Article
        @apiName Unlike
        @apiGroup Article

        @apiSuccess {Object} like Instance of Like
        """
        article = self.get_article(pk)
        like = Like.objects.filter(article=article, user=request.user).count()
        if like == 1:
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class RegisterUser(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
