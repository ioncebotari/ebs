from django.contrib.auth.models import User
from django.db import models


class Article (models.Model):
    """
    Definition of the Article model
    """

    title = models.CharField(max_length=100, blank=False, null=False)
    content = models.TextField(blank=False, null=False)
    likes = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        self.likes = Like.objects.filter(article=self).count()
        super(Article, self).save(*args, **kwargs)


class Like (models.Model):
    """
    Definition of the Like models
    """

    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        """
        Adding a Meta option to ensure that every user can like an article only once
        """
        unique_together = (("article", "user"),)
