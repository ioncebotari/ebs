from django.contrib.auth.models import User
from rest_framework import serializers
from news.models import Article, Like

"""
Serializers initialization for User, Article, Like classes
"""


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for Django user class
    """

    class Meta:
        """
        Meta class for Django user class
        """

        model = User
        fields = ('username', 'email', 'password')


class ArticleSummarySerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for the article preview
    """

    class Meta:
        """
        Meta class for Articles
        """

        model = Article
        fields = ('pk', 'title', 'content')


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer for full article presentation
    """

    class Meta:
        model = Article
        fields = ('title', 'content', 'likes')


class LikeSerializer(serializers.ModelSerializer):
    """
    Serializer for the Like class
    """
    article = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    user = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        """
        Meta class for Like
        """

        model = Like
        fields = ('article', 'user')
