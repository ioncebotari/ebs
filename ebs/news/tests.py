from django.test import LiveServerTestCase
from rest_framework.test import APIClient
from django.urls import resolve
from django.contrib.auth.models import User
from news.models import Article


class ViewTest(LiveServerTestCase):
    """Unit test for verifying the Views presence"""

    def test_articles_views(self):
        """Tests the presence of views for the List Articles functionality"""

        found = resolve('/articles/')
        self.assertEqual(found.view_name, 'news.views.ArticleList')

    def test_articles_list(self):
        """Tests the View Article view"""
        found = resolve('/articles/1/')
        self.assertEqual(found.view_name, 'news.views.ArticleDetail')

    def test_article_like(self):
        """Tests the Like article"""
        found = resolve('/articles/1/like/')
        self.assertEqual(found.view_name, 'news.views.LikeView')

    def test_users_auth(self):
        """Tests the user auth view presence"""
        found = resolve('/auth/')
        self.assertEqual(found.view_name, 'rest_framework.authtoken.views.ObtainAuthToken')

    def test_register(self):
        """Test the registration view"""
        found = resolve('/register/')
        self.assertEqual(found.view_name, 'news.views.RegisterUser')




class StatusTest(LiveServerTestCase):
    """Class used for testing response numbers from views"""

    def setUp(self):
        """Sets up one user and one article"""
        self.user1 = User.objects.create_user('test1', 'test1@test1.com', 'test1test1')
        self.article1 = Article(title='Article1', content='Content of Article1')
        self.article1.save()
        self.article2 = Article(title='Article2', content='Content of Article2')
        self.article2.save()
        self.user1.save()
        self.client = APIClient()

    def test_get_unauthorized_article(self):
        """Test that getting an article without authentication returns status code 403"""
        unauthorized_request = self.client.get('/articles/{}/'.format(self.article1.pk))
        self.assertEqual(unauthorized_request.status_code, 403)

    def test_get_authorized_article(self):
        """Test that getting an article with authorization returns status code 200"""
        self.client.login(username='test1', password='test1test1')
        authorized_request = self.client.get('/articles/{}/'.format(self.article1.pk))
        self.assertEqual(authorized_request.status_code, 200)
        self.assertContains(authorized_request, 'Content of Article1')

    def test_get_inexistent_article(self):
        """Test that accessing an inexistent article returns code 404"""
        self.client.login(username='test1', password='test1test1')
        authorized_request = self.client.get('/articles/9999999/')
        self.assertEqual(authorized_request.status_code, 404)

    def test_post_article(self):
        """Test posting a new article returns code 201"""
        self.client.login(username='test1', password='test1test1')
        request = self.client.post('/articles/', {'title': 'test', 'content': 'test'}, format='json')
        self.assertEqual(request.status_code, 201)

    def test_delete_article(self):
        """Test removing an article"""
        self.client.login(username='test1', password='test1test1')
        self.client.delete('/articles/{}/'.format(self.article2.pk))
        request = self.client.get('/articles/{}/'.format(self.article2.pk))
        self.assertEqual(request.status_code, 404)

    def test_put_like(self):
        """Test that PUTing and deleting a like works"""
        self.client.login(username='test1', password='test1test1')
        self.client.put('/articles/{}/like/'.format(self.article1.pk), data=None)
        request = self.client.get('/articles/{}/'.format(self.article1.pk))
        self.assertContains(request, '"likes":1')
        self.client.delete('/articles/{}/like/'.format(self.article1.pk))
        request = self.client.get('/articles/{}/'.format(self.article1.pk))
        self.assertContains(request, '"likes":0')

    def test_new_user(self):
        """Test registering a new user"""
        request = self.client.post('/register/', format='json', data={
            "username": "test",
            "email": "test@test.me",
            "password": "test"
        })
        self.assertEqual(request.status_code, 201)

    def test_get_token(self):
        """Test token generation"""
        request = self.client.post('/auth/', format='json', data={
            'username': 'test1',
            'password': 'test1test1'
        })
        self.assertEqual(request.status_code, 200)
        self.assertContains(request, 'token')
