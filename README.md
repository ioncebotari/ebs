# Test for EBS

## Installation:

1. Install Vagrant, Ansible and Virtualbox
2. Clone the repository to your local filesystem
3. Use the command ````vagrant up```` to run a VM that will contain the project’s environment
4. The project is available at the IP address 192.168.13.7, port 80. Use your browser and/or curl to access it.

## Testing

After the VM is up, use ````vagrant ssh```` to log into it. The project directory is at /vagrant/ebs. Change the current working directory there.

Use the command ````source venv/bin/activate```` to access the binaries from the virtualenv

Run ````manage.py test --settings=ebs.test_settings```` to run the automated tests.

